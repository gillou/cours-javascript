// Fonction pour changer la couleur de fond de la page
function changeBG() {
    // Génération d'un couleur aléatoire
    var monBody = document.getElementById("monBody")
    var strNewColor = genererCouleurRGB()
    monBody.style.backgroundColor = strNewColor
    document.getElementById("bgColor").innerHTML = "Couleur actuelle : "+strNewColor
}


function genererCouleurRGB() {
    // Fonction pour générer une couleur au format rgb(1,2,3)
    var [r,g,b] = [Math.floor(Math.random()*255), Math.floor(Math.random()*255), Math.floor(Math.random()*255)]
    return `rgb(${r},${g},${b})`; // "rgb("+r+","+g+","+b+")"
}

// Pas utilisé pour l'instant
function genererCouleurHexa() {
    // Fonction pour générer une couleur au format hexadécimal #FFFFFF
    const arHexa = Array.from("0123456789ABCDEF")
    var newBG = arHexa.sort(() => {return 0.75 - Math.random()}).slice(0,6)
    return "#"+newBG.join('');
}
